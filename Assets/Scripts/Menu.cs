﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	public static int selectedLevel;

	public GameObject levelSelector;
	public GameObject mainMenu;
	public GameObject[] levelBtn;
	public GameObject upgradePanel;
	public GameObject notEnoughMoney;

	public Text totalScoreTxt;
	public Text pointText;
	public Text[] levelBtnIndex;
	public Text resistanceCostTxt;
	public Text torqueCostTxt;
	public Text speedCostTxt;
	public Text accelerationCostTxt;
	public Text defenseCostTxt;

	public Button[] levelBtnBtn;

	public Image[] resistanceLevel;
	public Image[] torqueLevel;
	public Image[] maxSpeedLevel;
	public Image[] accelerationLevel;
	public Image[] defenseLevel;
	public Image soundSetting;

	public Button resistanceUpBtn;
	public Button torqueUpBtn;
	public Button speedUpBtn;
	public Button accelerationUpBtn;
	public Button defenseUpBtn;

	public Sprite upgraded;
	public Sprite soundOn, soundOff;
						
	public int[] resistanceCost;
	public int[] torqueCost;
	public int[] maxSpeedCost;
	public int[] accelerationCost;
	public int[] defenseCost;

	private	void Start ()
	{
//		PlayerPrefs.SetInt ("money", 300000);
//		PlayerPrefs.SetInt ("level", 14);
//		PlayerPrefs.DeleteAll ();
		SetScoreTxt ();
		SetLevelNumber ();
		CheckUpgradeStt ();
		SetUpgradeCostText ();
		UpgradeFull ();
		CheckSound ();
	}

	public void MainMenu(int index)
	{
		switch (index) {
		case 0:
			Application.OpenURL ("https://play.google.com/store");
			break;
		case 1:
			if (levelSelector == null) {
				Debug.Log ("Level Selector Is Null");
				return;
			}
			levelSelector.SetActive (true);
			mainMenu.SetActive (false);
			break;
		case 2:
			Debug.Log ("High Score");
			break;
		case 3:
			if (mainMenu == null) {
				Debug.Log ("Main Menu Is Null");
				return;
			}
			mainMenu.SetActive (true);
			levelSelector.SetActive (false);
			break;
		case 4:
			Application.LoadLevel (1);
			break;
		case 5:
			if (levelSelector == null) {
				Debug.Log ("Level Selector Is Null");
				return;
			}
			levelSelector.SetActive (true);
			upgradePanel.SetActive (false);
			break;
		}
	}
		
	public void SetScoreTxt()
	{
		totalScoreTxt.text = "Total Score: " + PlayerPrefs.GetInt ("totalscore").ToString ();
		pointText.text = "Money: " + PlayerPrefs.GetInt ("money").ToString ();
	}

	private void SetLevelNumber()
	{
		for (int i = 0; i < levelBtnIndex.Length; i++) {
			levelBtnIndex [i].text = (i + 1).ToString ();
			if (i <= PlayerPrefs.GetInt ("level")) {
				levelBtnIndex [i].gameObject.SetActive (true);
				levelBtn [i].SetActive (false);
				levelBtnBtn [i].interactable = true;
			}
		}
	}

	public void SelectLevel(int index)
	{
		selectedLevel = index;
		if (upgradePanel == null) {
			Debug.Log ("Upgrade Panel Is Null");
			return;
		}
		upgradePanel.SetActive (true);
		levelSelector.SetActive (false);
	}

	private void CheckUpgradeStt()
	{
		for (int i = 0; i < resistanceLevel.Length; i++) {
			if (i < PlayerPrefs.GetInt ("resistance")) {
				resistanceLevel [i].sprite = upgraded;
			}
			if (i < PlayerPrefs.GetInt ("torque")) {
				torqueLevel [i].sprite = upgraded;
			}
			if (i < PlayerPrefs.GetInt ("speed")) {
				maxSpeedLevel [i].sprite = upgraded;
			}
			if (i < PlayerPrefs.GetInt ("acceleration")) {
				accelerationLevel [i].sprite = upgraded;
			}
			if (i < PlayerPrefs.GetInt ("defense")) {
				defenseLevel [i].sprite = upgraded;
			}
		}
	}

	private void SetUpgradeCostText()
	{
		if(PlayerPrefs.GetInt ("resistance")<resistanceCost.Length)
			resistanceCostTxt.text = "$" + resistanceCost [PlayerPrefs.GetInt ("resistance")].ToString();
		if(PlayerPrefs.GetInt ("torque")<resistanceCost.Length)
			torqueCostTxt.text = "$" + torqueCost [PlayerPrefs.GetInt ("torque")].ToString();
		if(PlayerPrefs.GetInt ("speed")<resistanceCost.Length)
			speedCostTxt.text = "$" + maxSpeedCost [PlayerPrefs.GetInt ("speed")].ToString();
		if(PlayerPrefs.GetInt ("acceleration")<resistanceCost.Length)
			accelerationCostTxt.text = "$" + accelerationCost [PlayerPrefs.GetInt ("acceleration")].ToString();
		if(PlayerPrefs.GetInt ("defense")<resistanceCost.Length)
			defenseCostTxt.text = "$" + defenseCost [PlayerPrefs.GetInt ("defense")].ToString();
	}

	public void UpgradeOption(int index)
	{
		switch (index) {
		case 0://resistance
			if (PlayerPrefs.GetInt ("money") > resistanceCost [PlayerPrefs.GetInt ("resistance")]) {
				PlayerPrefs.SetInt ("money", PlayerPrefs.GetInt ("money") - resistanceCost [PlayerPrefs.GetInt ("resistance")]);
				if (PlayerPrefs.GetInt ("resistance") < resistanceCost.Length) {
					PlayerPrefs.SetInt ("resistance", PlayerPrefs.GetInt ("resistance") + 1);
				}
			} else {
				notEnoughMoney.SetActive (true);
			}
			break;
		case 1://torque
			if (PlayerPrefs.GetInt ("money") > resistanceCost [PlayerPrefs.GetInt ("torque")]) {
				PlayerPrefs.SetInt ("money", PlayerPrefs.GetInt ("money") - resistanceCost [PlayerPrefs.GetInt ("torque")]);
				if (PlayerPrefs.GetInt ("torque") < resistanceCost.Length) {
					PlayerPrefs.SetInt ("torque", PlayerPrefs.GetInt ("torque") + 1);
				}
			}
			else {
				notEnoughMoney.SetActive (true);
			}
			break;
		case 2://max speed
			if (PlayerPrefs.GetInt ("money") > resistanceCost [PlayerPrefs.GetInt ("speed")]) {
				PlayerPrefs.SetInt ("money", PlayerPrefs.GetInt ("money") - resistanceCost [PlayerPrefs.GetInt ("speed")]);
				if (PlayerPrefs.GetInt ("speed") < resistanceCost.Length) {
					PlayerPrefs.SetInt ("speed", PlayerPrefs.GetInt ("speed") + 1);
				}
			}
			else {
				notEnoughMoney.SetActive (true);
			}
			break;
		case 3://acceleration
			if (PlayerPrefs.GetInt ("money") > resistanceCost [PlayerPrefs.GetInt ("acceleration")]) {
				PlayerPrefs.SetInt ("money", PlayerPrefs.GetInt ("money") - resistanceCost [PlayerPrefs.GetInt ("acceleration")]);
				if (PlayerPrefs.GetInt ("acceleration") < resistanceCost.Length) {
					PlayerPrefs.SetInt ("acceleration", PlayerPrefs.GetInt ("acceleration") + 1);
				}

			}
			else {
				notEnoughMoney.SetActive (true);
			}
			break;
		case 4://defense
			if (PlayerPrefs.GetInt ("money") > resistanceCost [PlayerPrefs.GetInt ("defense")]) {
				PlayerPrefs.SetInt ("money", PlayerPrefs.GetInt ("money") - resistanceCost [PlayerPrefs.GetInt ("defense")]);
				if (PlayerPrefs.GetInt ("defense") < resistanceCost.Length) {
					PlayerPrefs.SetInt ("defense", PlayerPrefs.GetInt ("defense") + 1);
				}
			}
			else {
				notEnoughMoney.SetActive (true);
			}
			break;
		}

		SetScoreTxt ();
		CheckUpgradeStt ();
		SetUpgradeCostText ();
		UpgradeFull ();
	}

	private void UpgradeFull()
	{
		if (PlayerPrefs.GetInt ("resistance") == 4) {
			resistanceUpBtn.interactable = false;
		}
		if (PlayerPrefs.GetInt ("torque") == 4) {
			torqueUpBtn.interactable = false;
		}
		if (PlayerPrefs.GetInt ("speed") == 4) {
			speedUpBtn.interactable = false;
		}
		if (PlayerPrefs.GetInt ("acceleration") == 4) {
			accelerationUpBtn.interactable = false;
		}
		if (PlayerPrefs.GetInt ("defense") == 4) {
			defenseUpBtn.interactable = false;
		}
	}

	private void CheckSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0) {
			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.volume = 1;
			soundSetting.sprite = soundOn;
		} else if (PlayerPrefs.GetInt ("sound") == 1) {
			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.volume = 0;
			soundSetting.sprite = soundOff;
		}
	}

	public void SetSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0) {
			PlayerPrefs.SetInt ("sound", 1);
		} else if (PlayerPrefs.GetInt ("sound") == 1) {
			PlayerPrefs.SetInt ("sound", 0);
		}
		CheckSound ();
	}
	public void ViewRewardVideo()
	{
		AdsControl.Instance.ShowRewardVideo ();
		SetScoreTxt ();
	}
	public void CloseNotEnoughMoneyPanel()
	{
		notEnoughMoney.SetActive (false);
		SetScoreTxt ();
	}
}
