﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public static GameManager instance { get; private set;}
	public int power = 100;
	public GameObject bloodPanel;
	public int score;
	public int bonus;

	public GameObject finishGamePanel,pausePanel;
	public bool isGameFInish;
	public bool isGameWin;

	public CarController carControl;
	private bool moveForWard, moveBackWard;
	private bool rotateClockWire, rotateCounterClockWire;

	#region Game Param Update
	public float[] maxSpeed;
	public float[] rotationSpeed;
	public float[] acceleration;
	public int[] defenseUpgrade;
	public float[] resistanceUpgrade;
	#endregion

	private GameObject map;

	private	void Awake ()
	{
		instance = this;	
		LoadMap ();
	}
	void Start ()
	{
		InvokeRepeating ("CountDownPower", 0.5f, 0.5f);
		InvokeRepeating ("CountDownBonus", 0, 0.2f);
		CheckSound ();

	}

	void Update ()
	{
		//------------------------------------------------------------
		if (isGameFInish) {
			isGameFInish = false;
			CancelInvoke ("CountDownPower");
			CancelInvoke ("CountDownBonus");
		}
//------------------------------------------------------------
		if (moveForWard) {
			carControl.movementInput = 1;
		} else if (moveBackWard) {
			carControl.movementInput = -1;
		} else {
			carControl.movementInput = 0;
		}

		if (rotateClockWire) {
			carControl.rotationInput = 1;
		} else if (rotateCounterClockWire) {
			carControl.rotationInput = -1;
		} else {
			carControl.rotationInput = 0;
		}

	}
	void CountDownPower()
	{
		if (power > 0) {
			power -= 1;
			UIManager.instance.SetPower ();
		} else {
			Time.timeScale = 0;
			WinGame (false);
			CancelInvoke ("CountDownPower");
		}
	}
	void CountDownBonus()
	{
		bonus -= 1;
		UIManager.instance.SetText (-1, "bonus");
	}

	public IEnumerator DisableBloodPanel()
	{
		yield return new WaitForSeconds (1);
		bloodPanel.SetActive (false);
	}


	private void CheckSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0) {
			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.volume = 1;
		} else if (PlayerPrefs.GetInt ("sound") == 1) {
			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.volume = 0;
		}
		UIManager.instance.SoundSetting ();
	}
	public void InGameFunction(int index)
	{
		switch (index) {
		case 0:
			Application.LoadLevel (Application.loadedLevel);
			break;
		case 1:
			if (pausePanel == null)
				return;
			Time.timeScale = 0;
			pausePanel.SetActive (true);
			AdsControl.Instance.showAds ();
			break;
		case 2:
			if (PlayerPrefs.GetInt ("sound") == 0) {
				PlayerPrefs.SetInt ("sound", 1);
			} else if (PlayerPrefs.GetInt ("sound") == 1) {
				PlayerPrefs.SetInt ("sound", 0);
			}
			CheckSound ();
			UIManager.instance.SoundSetting ();
			break;
		case 3:
			Application.LoadLevel (0);
			break;
		case 4:
			Time.timeScale = 1;
			pausePanel.SetActive (false);
			break;
		case 5:
			Time.timeScale = 1;
			Application.LoadLevel (Application.loadedLevel);
			break;
		case 6:
			Time.timeScale = 1;
			Application.LoadLevel (0);
			break;
		}
	}

	public void WinGame(bool isGameWin)
	{
		if (isGameWin) {
			UIManager.instance.UILevelComplete ();
		} else {
			UIManager.instance.UILevelFalse ();
		}
		finishGamePanel.SetActive (true);
	}

	public void EndGameFunction(int index)
	{
		switch (index) {
		case 0:
			Application.OpenURL("https://play.google.com/store");
			break;
		case 1:
			if (isGameWin) {
				Menu.selectedLevel += 1;
				Time.timeScale = 1;
				Application.LoadLevel (Application.loadedLevel);
			} else if (!isGameWin) {
				Time.timeScale = 1;
				Application.LoadLevel (Application.loadedLevel);
			}
			break;
		case 2:
			Time.timeScale = 1;
			Application.LoadLevel (0);
			break;
		default:
			break;
		}
	}

	///<Summary>
	///Mobile Input
	///</Summary>
	public void MobileInput(int index)
	{
		switch (index) {
		case 0:
			moveForWard = true;
			break;
		case 1:
			moveBackWard = true;
			break;
		case 2:
			rotateClockWire = true;
			break;
		case 3:
			rotateCounterClockWire = true;
			break;
		}
	}

	public void ReleaseBtn()
	{
		if (moveForWard) {
			moveForWard = false;
		}
		if (moveBackWard) {
			moveBackWard = false;
		}
		if (rotateClockWire) {
			rotateClockWire = false;
		}
		if (rotateCounterClockWire) {
			rotateCounterClockWire = false;
		}
	}

	private void LoadMap()
	{
		map = Instantiate (Resources.Load ("Level" + (Menu.selectedLevel + 1).ToString()))as GameObject;
	}
}
