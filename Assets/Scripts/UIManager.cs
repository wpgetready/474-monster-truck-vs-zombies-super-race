﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PointRoad
{
	public float startPoint;
	public float endPoint;
}

public class UIManager : MonoBehaviour {
	public static UIManager instance { get; private set;}
	[Header("Power Bar")]
	public Image powerImg;
	public Text powerTxt;
	public Text moneyText;
	public Text scoreText;
	public Text bonusText;

	public Slider roadSimulator;
	public PointRoad[] pointToCaculate;
	private float realDistance;
	public GameObject myTruck;

	public Image soundImg;
	public Sprite soundOn;
	public Sprite soundOff;

	public Text levelNoticeText;
	public Text socreTotalText;
	public Text retryAndNextText;
	public Text currentStateTxt;

	private	void Awake ()
	{
		instance = this;	
	}

	void Start ()
	{
		SetText (0,"money");	
		SetText (0, "score");
		SetText (0, "bonus");

		realDistance = Vector2.Distance (new Vector2 (pointToCaculate [Menu.selectedLevel].startPoint, 0), new Vector2 (pointToCaculate [Menu.selectedLevel].endPoint, 0));


		InvokeRepeating ("RoadSimulate", 0, 0.02f);

		SoundSetting ();

		currentStateTxt.text = "State: "+ (Menu.selectedLevel + 1).ToString ();
	}
		

	public void SetPower()
	{
		powerImg.fillAmount = GameManager.instance.power / 100f;
		powerTxt.text = "Power:" + GameManager.instance.power.ToString ();
	}

	public void SetText(int value, string type)
	{
		switch (type) {
		case "money":
			PlayerPrefs.SetInt ("money", PlayerPrefs.GetInt ("money") + value);
			moneyText.text = PlayerPrefs.GetInt ("money").ToString ();
			break;
		case "score":
			GameManager.instance.score += value;
			scoreText.text = GameManager.instance.score.ToString ();
			break;
		case "bonus":
			bonusText.text = GameManager.instance.bonus.ToString ();
			break;
		}
	}

	void RoadSimulate()
	{
		float currentDistance = Vector2.Distance (new Vector2 (myTruck.transform.position.x, 0), new Vector2 (pointToCaculate [Menu.selectedLevel].startPoint, 0));
		roadSimulator.value = currentDistance / realDistance;
	}

	public void SoundSetting()
	{
		if (PlayerPrefs.GetInt ("sound") == 0) {
			soundImg.sprite = soundOn;
		} else if (PlayerPrefs.GetInt ("sound") == 1) {
			soundImg.sprite = soundOff;
		}
	}

	public void UILevelComplete()
	{
		AdsControl.Instance.showAds ();
		levelNoticeText.text = "Level Complete";
		socreTotalText.text = "Score:" + (GameManager.instance.score + GameManager.instance.bonus).ToString ();
		retryAndNextText.text = "Next Level";
	}
	public void UILevelFalse()
	{
		AdsControl.Instance.showAds ();
		levelNoticeText.text = "Level False";
		socreTotalText.text = "Score:" + GameManager.instance.score.ToString ();
		retryAndNextText.text = "Retry";
	}
}
