﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
	public Rigidbody2D rig;
	public float xvelocity;
	public SpriteRenderer spr;
	public bool isDie;

	void Update ()
	{
		if (!isDie) {
			if (spr.isVisible) {
				rig.velocity = new Vector2 (xvelocity, rig.velocity.y);
			}
		}
	}
}

