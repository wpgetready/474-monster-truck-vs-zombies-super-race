﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public static SoundManager instance;

	public AudioSource sourceMusic;
	public AudioClip backgroundMusic;
	public AudioClip getMoney;
	public AudioClip[] zombieDead;
	public AudioClip winClip;
	private	void Awake ()
	{
		if (instance == null) {
			DontDestroyOnLoad (this.gameObject);
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}

	void Start ()
	{
		sourceMusic.clip = backgroundMusic;
		sourceMusic.Play ();
		sourceMusic.loop = true;
	}
}
