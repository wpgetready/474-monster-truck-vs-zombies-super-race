using UnityEngine;
using System.Collections;

public class CarController : MonoBehaviour {

	private float speed;
	public float rotationSpeed = 15f;

	public WheelJoint2D backWheel;
	public WheelJoint2D frontWheel;

	public Rigidbody2D rb;

	private float movement = 0f;
	private float rotation = 0f;



	public float movementInput;
	public float rotationInput;

	public GameObject fire;
	void Update ()
	{
//		movement = -Input.GetAxisRaw("Vertical") * speed;
//		rotation = Input.GetAxisRaw("Horizontal");

		movement = movementInput*GameManager.instance.maxSpeed[PlayerPrefs.GetInt ("speed")];
//		movement = movementInput*Mathf.Lerp(backWheel.motor.motorSpeed, GameManager.instance.maxSpeed[PlayerPrefs.GetInt ("speed")],GameManager.instance.acceleration[PlayerPrefs.GetInt ("acceleration")]);
		rotation = rotationInput;
	}

	void FixedUpdate ()
	{
		if (movement == 0f)
		{
			backWheel.useMotor = false;
			frontWheel.useMotor = false;
		} else
		{
			backWheel.useMotor = true;
			frontWheel.useMotor = true;

			JointMotor2D motor = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
			backWheel.motor = motor;
			frontWheel.motor = motor;
		}
			
		rb.AddTorque(-rotation * GameManager.instance.maxSpeed[PlayerPrefs.GetInt ("torque")] * Time.fixedDeltaTime);
    }
	int pointAdd;
	void OnTriggerEnter2D (Collider2D col)
	{


		if (col.tag == "money") {
			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.PlayOneShot (SoundManager.instance.getMoney);
			switch (Random.RandomRange (0, 3)) {
			case 0:
				pointAdd = 5;
				break;
			case 1:
				pointAdd = 10;
				break;
			case 2:
				pointAdd = 15;
				break;

			}
			var poinText = col.gameObject.transform.parent.GetChild (1).transform.gameObject.GetComponent<TextMesh> ();
			poinText.text = "+" + pointAdd.ToString () + "$";
			UIManager.instance.SetText (pointAdd, "money");
			UIManager.instance.SetText (25, "score");

			poinText.gameObject.SetActive (true);
			col.gameObject.SetActive (false);

		} else if (col.tag == "superpower") {
			rb.velocity = new Vector2 (10, 5);
			fire.SetActive (true);
			Invoke ("StopFire", 2);
//			speed = 3000;
//			Invoke ("NormalizeSpeed", 2);
			col.gameObject.SetActive (false);
		} else if (col.tag == "finishflag") {
			GameManager.instance.isGameFInish = true;
			GameManager.instance.isGameWin = true;
			GameManager.instance.WinGame (true);
			if (PlayerPrefs.GetInt ("level") == Menu.selectedLevel) {
				PlayerPrefs.SetInt ("level", PlayerPrefs.GetInt ("level") + 1);
			}

			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.PlayOneShot (SoundManager.instance.winClip);

		}
	}
	private	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "enemy") {
			if (SoundManager.instance == null)
				return;
			SoundManager.instance.sourceMusic.PlayOneShot (SoundManager.instance.zombieDead [Random.Range (0, 2)]);

			col.gameObject.SetActive (false);
			GameManager.instance.bloodPanel.SetActive (true);
			StartCoroutine (GameManager.instance.DisableBloodPanel());

			UIManager.instance.SetText (GameManager.instance.defenseUpgrade[PlayerPrefs.GetInt ("defense")], "score");
		}
	}

	void NormalizeSpeed()
	{
		speed = GameManager.instance.maxSpeed[PlayerPrefs.GetInt ("speed")];
	}

	void StopFire()
	{
		fire.SetActive (false);
	}
}
