﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadTrigger : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "platform") {
			Time.timeScale = 0;
			GameManager.instance.WinGame (false);
		}
	}
}
